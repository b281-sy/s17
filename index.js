 /*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function userProfile(){
        let fullName = prompt("Enter your full name: ");
        let age = prompt("How old are you? ");
        let location = prompt("What city are you located?")
    
        console.log("Hello, " + fullName);
        console.log("You are " + age + " years old.");
        console.log("You live in " + location)
    };

    userProfile();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function displayTopBandList(){
        console.log("1. Black Pink");
        console.log("2. Gigi De Lana and the GiGi Vibes");
        console.log("3. Parokya ni Edgar ");
        console.log("4. Backstreet Boys");
        console.log("5. Spice Girls");
    };

    displayTopBandList();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function displayTopMoviesList(){
        console.log("1. The Notebook");
        console.log("Rotten Tomato Rating: 52%");
        console.log("2. Avatar");
        console.log("Rotten Tomato Rating: 81%");
        console.log("3. Avatar: The Way of Water");
        console.log("Rotten Tomato Rating: 76%");
        console.log("4. Titanic");
        console.log("Rotten Tomato Rating: 89%");
        console.log("5. The Queen's Gambit");
        console.log("Rotten Tomato Rating: 96%");
    };

    displayTopMoviesList();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();